"use strict"

const fs = require("fs")
const path = require("path")
const {
    Controller,
    Tag,
    TagGroup
} = require("ethernet-ip") // https://www.npmjs.com/package/ethernet-ip

// Nowy obiekt
const PLC = new Controller()
let dataFromPlc = new Array()
let intervalId = 0
let plcProgram = null

// Pobranie tagów z pliku json
const fileWithTags = fs.readFileSync(path.join(__dirname, '../json/tagi.json'))
const tagsJson = JSON.parse(fileWithTags)

// Pobranie ustawień połączenia z pliku json
const fileWithConfig = fs.readFileSync(path.join(__dirname, '../json/config.json'))
const cfg = JSON.parse(fileWithConfig)

// zasubskrybowanie tagów z pliku do cyklicznego odczytu ze sterownika
const group = new TagGroup()
for (let oneTag of tagsJson) {
    if (oneTag.progr != null) plcProgram = oneTag.progr // dodanie wartości podprogramu sterownika
    group.add(new Tag(oneTag.name, oneTag.progr))
}

PLC.connect(cfg.ip, cfg.slot).then(async () => {
    console.log(`\n\nConnected to PLC ${PLC.properties.name}...\n`);

    clearInterval(intervalId) // resetetuj timer za każdym połączeniem
    intervalId = setInterval( async () => {
        dataFromPlc = []
        try {
            await PLC.readTagGroup(group)
        } catch (err) {
            console.error(err)
        }

        group.forEach(tag => { // odczytanie danych z plc i stworzenie tablicy która zostanie wysłana do przeglądarki przez socketIO
            let field = {}
            let tempName = tag.name.replace(`Program:${plcProgram}.`, ""); // pozbycie się przedrostka np. dla podprogramu KLAPY: "Program:KLAPY."

            field.name = tempName
            field.val = tag.value
            dataFromPlc.push(field)
        });
        //console.log(dataFromPlc)
        exports.dataFromPlc = dataFromPlc // Udostępnienie danych dla serwera www (index.js)
    }, cfg.scanRate) // odświeżanie zadane z pliku config.json


});


//exports.cfg.scanRate