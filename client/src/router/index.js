import Vue from 'vue'
import Router from 'vue-router'
import DataTable from '@/components/DataTable'
import Help from '@/views/Help.vue'
import Info from '@/views/Info.vue'

Vue.use(Router)

export default new Router ({
    routes: [
        {
            path: '/',
            name: 'DataTable',
            component: DataTable
        },
        {
            path: '/info',
            name: 'Info',
            component: Info
        },
        {
            path: '/help',
            name: 'Help',
            component: Help
        }
    ]

})