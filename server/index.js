"use strict"

const express = require('express')
const cors = require('cors')
const path = require('path')
const PLC = require('./ethip/eth') // wywołanie komunikacji po ethernetIP z PLC Rockwell Automation

// Ustawienia serwera
const app = express()
const port = process.env.PORT || 5000
app.use(express.static(path.join(__dirname, '/public/'))) // pliki statyczne klienta
app.get(/.*/, (req, res) => res.sendfile(__dirname + '/public/index.html')) // SPA - Single Page Application
app.use(cors()) // Middleware - Disable Chrome Origin Policy
// Start serwera
const server = app.listen(port, () => {
    console.log(`Server started on port ${port}`)
})

// udostęnienie danych dla klienta (przeglądarki) po socketIO
const io = require('socket.io')(server);
let intervalId = 0
io.on('connection', function (socket) {
    console.log('\nSocketIO user connected, id: ' + socket.id)
    socket.on('disconnect', () => {
        console.log('SocketIO user disconnected, id: ' + socket.id)
    })

    clearInterval(intervalId)
    intervalId = setInterval(() => {
        io.emit('dane', PLC.dataFromPlc)
        //console.log(PLC.dataFromPlc)
    }, 1000)

});



